if exists("g:loaded_opensourceonweb") || &cp
	finish
endif
let g:loaded_opensourceonweb = 1

let s:opensourceonweb_plugin_dir=expand('<sfile>:p:h')

nnoremap <unique> <script> <silent> <plug>OpenSourceOnWeb       :call opensourceonweb#opensourceonweb()<cr>
vnoremap <unique> <script> <silent> <plug>OpenSourceOnWebVisual :call opensourceonweb#opensourceonweb("v")<cr>

" default mapping is like this:
if !hasmapto('<plug>OpenSourceOnWeb')
	nmap <leader>gl <plug>OpenSourceOnWeb
endif
if !hasmapto('<plug>OpenSourceOnWebVisual')
	vmap <leader>gl <plug>OpenSourceOnWebVisual
endif

let g:opensourceonweb_if_webserver_not_configured_try_origin_server_name =
		\ get( g:, 'opensourceonweb_if_webserver_not_configured_try_origin_server_name', 1 )

" example:
" vnoremap <F4> :'<,'>call opensourceonweb#opensourceonweb("v")<cr>

command! OpenSourceOnWeb call opensourceonweb#opensourceonweb()
" command -range OpenSourceOnWeb :call opensourceonweb#opensourceonweb(<line1>, <line2>)
command! -range OpenSourceOnWeb :call opensourceonweb#opensourceonweb("v")

" open git source on gitlab server
function! opensourceonweb#opensourceonweb(...) range
	let l:mode = a:0 > 0 ? "v" : "n"

	if l:mode ==? "v"
		let l:linenum_from = getpos("'<")[1]
		let l:linenum_to = getpos("'>")[1]
	else
		let l:linenum_from = line('.')
		let l:linenum_to = ""
	endif

	echom s:opensourceonweb_plugin_dir . "/../third_party/opensourceonweb/bin/opensourceonweb " . expand("%") . " " . l:linenum_from . " " . l:linenum_to
	let l:url = system(s:opensourceonweb_plugin_dir . "/../third_party/opensourceonweb/bin/opensourceonweb " . expand("%") . " " . l:linenum_from . " " . l:linenum_to)
	echom "Loading: ".l:url

	" launch browser
	call system("(firefox -remote 'ping()' && firefox -remote 'openURL(".l:url.")') >/dev/null 2>&1 \|\| firefox '".l:url."' >/dev/null 2>&1")
endfunction

"function opensourceonweb#gogitlab()
"	if mode() !~# "^[vV\<C-v>]"
"		echom "Not visual"
"		call opensourceonweb#gitlab()
"	else
"		echom "Visual"
"		call opensourceonweb#gitlab("v")
"	endif
"endfunction
"noremap <expr> ,gl GoGitlab()
