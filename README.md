Open git source (file, current line or visual block) on gitlab server using a web browser.

Default mapping:

```
map <leader>gl <plug>OpenSourceOnWeb
vmap <leader>gl <plug>OpenSourceOnWebVisual
```

Default commands:

```
:OpenSourceOnWeb
:'<,'>OpenSourceOnWeb
```
